package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users ORDER BY id";
	private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";

	private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";
	private static final String SQL_FIND_TEAM_BY_NAME = "SELECT * FROM teams WHERE name=?";

	private static final String SQL_CREATE_USER = "INSERT INTO users VALUES (DEFAULT , ?)";
	private static final String SQL_CREATE_TEAM = "INSERT INTO teams VALUES (DEFAULT , ?)";

	private static final String SQL_DELETE_USER = "DELETE FROM users WHERE id=?";
	private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE id=?";

	private static final String SQL_UPDATE_TEAM = "update teams set name=? where id=?";
	private static final String SQL_CREATE_USER_TEAM = "INSERT INTO users_teams VALUES (?, ?)";
	private static final String SQL_FIND_ALL_TEAMS_BY_USER = "SELECT * FROM teams WHERE teams.id = ANY (SELECT team_id FROM users_teams WHERE user_id=?)";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();

		}
		return instance;
	}

	private DBManager() {

	}

	public List<User> findAllUsers() {
		List<User> users = new ArrayList<>();
		Connection con = null;
		Statement stmt;
		ResultSet rs;
		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
			while (rs.next()) {
				users.add(extractUser(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return users;
	}
	public List<Team> findAllTeams() {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		Statement stmt;
		ResultSet rs;
		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS);
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return teams;
	}

	private User extractUser(ResultSet rs) {
		User user = new User();

		try {
			user.setId(rs.getInt("id"));
			user.setLogin(rs.getString("login"));
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return user;
	}
	private Team extractTeam(ResultSet rs){
		Team team = new Team();
		try {
			team.setId(rs.getInt("id"));
			team.setName(rs.getString("name"));
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}

		return team;
	}

	public boolean insertUser(User user) {
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(
					SQL_CREATE_USER,
					Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			pstmt.setString(k++, user.getLogin());
			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					user.setId(rs.getInt(1));
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return false;
	}
	public boolean insertTeam(Team team) {
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(
					SQL_CREATE_TEAM,
					Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			pstmt.setString(k++, team.getName());
			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					team.setId(rs.getInt(1));
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return false;
	}

	public void deleteUser(int userId) {
		Connection con = null;
		PreparedStatement pstmt;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_USER);

			int k = 1;
			pstmt.setInt(k++, userId);

			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
	}

	public boolean deleteTeam(Team team) {
		Connection con = null;
		PreparedStatement pstmt;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_TEAM);

			int k = 1;
			pstmt.setInt(k++, team.getId());

			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return false;
	}

	public boolean deleteUsers(User... users) {
		Connection con = null;
		PreparedStatement pstmt;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_USER);

			int k = 1;
			pstmt.setInt(k++, users[0].getId());

			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return false;
	}

	public User getUser(String login) {
		User user = null;
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
			pstmt.setString(1, login);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extractUser(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return user;
	}
	public Team getTeam(String name) {
		Team team = null;
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_TEAM_BY_NAME);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				team = extractTeam(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return team;

	}

	public List<Team> getUserTeams(User user) {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt;
		ResultSet rs;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_ALL_TEAMS_BY_USER);
			pstmt.setInt(1, user.getId());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				teams.add(extractTeam(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return teams;
	}
	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			for (Team team : teams) {
				PreparedStatement pstmt =
						con.prepareStatement(SQL_CREATE_USER_TEAM);
				pstmt.setInt(1, user.getId());
				pstmt.setInt(2, team.getId());
				pstmt.executeUpdate();
			}
			con.commit();
		} catch (SQLException ex) {
			ex.printStackTrace();
			rollback(con);
			throw new DBException("error in setTeamsForUser", ex);
		} finally {
			close(con);
		}
		return true;
	}

	private void rollback(Connection con) {
		try {
			if (con != null) {
				con.rollback();
			}
		} catch (SQLException e) {
			// just write to log
			e.printStackTrace();
		}
	}

	public boolean updateTeam(Team team) {
		Connection con = null;
		PreparedStatement pstmt;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_TEAM);

			int k = 1;
			pstmt.setString(k++, team.getName());
			pstmt.setInt(k++, team.getId());

			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		return false;
	}

	private Connection getConnection() {
		Connection con =
				null;
		try {
			con = DriverManager.getConnection(getURL());
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return con;
	}

	private static String getURL() {

		String URL = null;
		try (InputStream input = new FileInputStream("app.properties");) {
			Properties prop = new Properties();
			prop.load(input);
			URL = prop.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}

		return URL;
	}

	private void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
