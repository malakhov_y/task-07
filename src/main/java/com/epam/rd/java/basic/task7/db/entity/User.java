package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		User user = new User();
		user.setLogin(login);
		user.setId(0);
		return user;
	}

	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object other) {
		if (this.getClass() != other.getClass()) return false;
		User otherObj = (User) other;
		return Objects.equals(this.getLogin(), otherObj.getLogin());
	}
}

